public class Board 
{
    private Enum[][] tictactoeBoard;

    

    public Board()
    {
        this.tictactoeBoard = new Enum[3][3];
        for (int i = 0; i < tictactoeBoard.length; i++)
        {
            for (int h = 0; h < tictactoeBoard.length; h++)
            {
                this.tictactoeBoard[i][h] = Square.BLANK;
            }
        }
    }

    

    public String toString()
    {
        String output = "  0 1 2";
        for (int i = 0; i < tictactoeBoard.length; i++)
        {
            output = output + "\n" + i;
            for (int h = 0; h < tictactoeBoard.length; h++)
            {
                output = output + " " + this.tictactoeBoard[i][h];
            }
            
        }
        return output;
    }

    

    public boolean placeToken(int row, int col, Square playToken)
    {
        int boardLength = tictactoeBoard.length - 1;
        if (row > boardLength || col > boardLength)
        {
            return false;
        }

        if (tictactoeBoard[row][col] == Square.BLANK)
        {
            tictactoeBoard[row][col] = playToken;
            return true;
        }
        else
        {
            return false;
        }
    }

    public boolean checkIfFull()
    {   
        int cases = tictactoeBoard.length * tictactoeBoard.length;
        int count = 0;
        for (int i = 0; i < tictactoeBoard.length; i++)
        {
            for (int h = 0; h < tictactoeBoard.length; h++)
            {
                if(tictactoeBoard[i][h] != Square.BLANK)
                {
                    count++;
                }
            }
        }
        if(count == cases)
        {
            return true;
        }
        return false;
    }

    private boolean checkIfWinningHorizontal(Square playerToken)
    {
        for (int i = 0; i < tictactoeBoard.length; i++)
        {
            int winningNum = 0;
            for (int h = 0; h < tictactoeBoard.length; h++)
            {
                if (tictactoeBoard[i][h] == playerToken)
                {
                    winningNum++;
                }
            }
            if(winningNum >= tictactoeBoard.length)
            {
                return true;
            }
        }
        return false; 
    }

    private boolean checkIfWinningVertical(Square playerToken)
    {
        for (int i = 0; i < tictactoeBoard.length; i++)
        {
            int winningNum = 0;
            for (int h = 0; h < tictactoeBoard.length; h++)
            {
                if (tictactoeBoard[h][i] == playerToken)
                {
                    winningNum++;
                }
            }
            if(winningNum >= tictactoeBoard.length)
            {
                return true;
            }
        }
        return false; 
    }

    public boolean checkIfWinning(Square playerToken)
    {
        boolean win = checkIfWinningHorizontal(playerToken);
        if (win)
        {
            return true;
        }
        else
        {
            win = checkIfWinningVertical(playerToken);
            if (win)
            {
                return true;
            }
        }
        return false;

    }
}
